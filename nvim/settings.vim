" Settings

filetype on
filetype plugin on
filetype indent on
syntax on
set nocompatible
set number
set shiftwidth=4
set tabstop=4
set nobackup
set nowritebackup
set updatetime=300
set scrolloff=10
set nowrap
set incsearch
set showcmd
set showmode
set showmatch
set encoding=utf-8
set hidden
set background=dark 
colorscheme gruvbox
let g:airline_powerline_fonts = 1
