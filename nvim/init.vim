" Settings

source $HOME/.config/nvim/settings.vim

" Plugins

source $HOME/.config/nvim/plugins.vim

" Mappings

 source $HOME/.config/nvim/mappings.vim
